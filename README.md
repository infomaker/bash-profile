## .devops

#### Usage
When logged in to one of our instances simply type `. .devops` to load the bash profile. If the profile was loaded correctly your terminal prompt should have changed
to something similar like this ` ec2-user lab-i-oc243edit-backend`

If the profile is missing on the instance you can download it by running
`curl -s https://infomaker-public-lambdas.s3-eu-west-1.amazonaws.com/bash-profile/.devops --output /home/ec2-user/.devops`

The profile might exist but has not been updated for a while, you can force an update by running `updateprofile` after you have loaded the bash profile initially with `. .devops`

By default the profile will be located in the ec2-user home folder. You can add the profile in the `/root/` folder by running `linkprofile` you can then type `sudo su -` followed by `. .devops` your prompt should now change to something similar like this
`root lab-i-oc243edit-backend`

#### Example of aliases available
`oc-mysql` will instantly start the mysql client and load the opencontent database for you.

`tailsolr` will be the equivalent of running `tail -F /opt/opencontent/solr/logs/solr.log'`

`restartopencontent` is the equivalent of running `sudo systemctl restart opencontent.target`

For more available aliases please see  the [.devops](.devops) file